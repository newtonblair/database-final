<?php

include('connection.txt');

$conn = mysqli_connect($server, $user, $pass, $dbname, $port)
or die('Error connecting to MySQL server.');

?>

<html>
  <head>
    <title>Another Simple PHP-MySQL Program</title>
    <style>
      table, td {
      border: 1px solid black;
      }
      th{
        border: 1px solid black;
        height: 20px;
      }
    </style>
  </head>
  
  <body bgcolor="white">
  
  
  <hr>
  
  
<?php
$emp_ssn = $_POST['emp'];

$query = "SELECT m.complex_id, m.unit_num, m.description
FROM mydb.Carpenter AS c1 JOIN mydb.Maintenance_Request AS m USING(emp_ssn)
  JOIN mydb.Unit AS u USING(unit_num, complex_id)
WHERE c1.emp_ssn = ".$emp_ssn." AND m.complex_id NOT IN(
  SELECT comp.complex_id
  FROM mydb.Carpenter AS c JOIN mydb.Works AS w USING(emp_ssn)
    JOIN mydb.Complex AS comp USING(complex_id)
  WHERE c.emp_ssn = ".$emp_ssn."
  );";

?>

<p>
The query:
<p>
<?php
print $query;
?>

<hr>
<p>
Result of query:
<p>

<?php
$result = mysqli_query($conn, $query)
or die(mysqli_error($conn));


print "<pre>";
print "<table align='center'>";
print "<tr>";
print "<th>Complex ID</th>";
print "<th>Unit #</th>";
print "<th>Request Description</th>";
print "</tr>";
while($row = mysqli_fetch_array($result, MYSQLI_BOTH))
  {
    print "<tr>";
    print "<th>$row[complex_id]</th> <th>$row[unit_num]</th> <th>$row[description]</th>";
    print "</tr>";
  }
print "</table>";
print "</pre>";

mysqli_free_result($result);

mysqli_close($conn);

?>

<p>
<hr>

<p>
<a href="BadMaintReqs.txt" >Contents</a>
of the PHP program that created this page. 	 
 
</body>
</html>