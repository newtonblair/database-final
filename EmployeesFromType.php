<?php

include('connection.txt');

$conn = mysqli_connect($server, $user, $pass, $dbname, $port)
or die('Error connecting to MySQL server.');

?>

<html>
  <head>
    <title>Another Simple PHP-MySQL Program</title>
    <style>
      table, td {
      border: 1px solid black;
      }
      th{
        border: 1px solid black;
        height: 20px;
      }
    </style>
  </head>
  
  <body bgcolor="white">
  
  
  <hr>
  
  
<?php
//Grab the double value from the form
$val = $_POST['emp_type'];
//explode to turn into array for accessing all values and determine size of array for being able to print extra attr for office
$val_arr = explode('|', $val);
$arr_len = count($val_arr);

$sub_group = $val_arr[0];
$pay_type = $val_arr[1];

if(3 == $arr_len){
  $position = $val_arr[2];

  $query = "SELECT e.ssn, e.fname, e.lname, e.sex, e.age, sub.".$pay_type.", sub.".$position.
  " FROM mydb.Employee AS e JOIN mydb.".$sub_group." AS sub ON e.ssn = sub.emp_ssn";
}
else{
  $query = "SELECT e.ssn, e.fname, e.lname, e.sex, e.age, sub.".$pay_type.
  " FROM mydb.Employee AS e JOIN mydb.".$sub_group." AS sub ON e.ssn = sub.emp_ssn";
}

?>

<p>
The query:
<p>
<?php
print $query;
?>

<hr>
<p>
Result of query:
<p>

<?php
$result = mysqli_query($conn, $query)
or die(mysqli_error($conn));

if(3 == $arr_len){
  print "<pre>";
  print "<table style='width:100%'>";
  print "<tr>";
  print "<th>ssn</th>";
  print "<th>Firstname</th>";
  print "<th>Lastname</th>";
  print "<th>Sex</th>";
  print "<th>Age</th>";
  print "<th>Salary</th>";
  print "<th>Position</th>";
  print "</tr>";
  while($row = mysqli_fetch_array($result, MYSQLI_BOTH))
    {
      print "<tr>";
      print "<th>$row[ssn]</th> <th>$row[fname]</th> <th>$row[lname]</th> <th>$row[sex]</th> <th>$row[age]</th> <th>$row[$pay_type]</th> <th>$row[position]</th>";
      print "</tr>";
    }
  print "</table>";
  print "</pre>";
}
else{
  print "<pre>";
  print "<table style='width:100%'>";
  print "<tr>";
  print "<th>ssn</th>";
  print "<th>Firstname</th>";
  print "<th>Lastname</th>";
  print "<th>Sex</th>";
  print "<th>Age</th>";
  print "<th>Hourly</th>";
  print "</tr>";
  while($row = mysqli_fetch_array($result, MYSQLI_BOTH))
    {
      print "<tr>";
      print "<th>$row[ssn]</th> <th>$row[fname]</th> <th>$row[lname]</th> <th>$row[sex]</th> <th>$row[age]</th> <th>$row[$pay_type]</th>";
      print "</tr>";
    }
  print "</table>";
  print "</pre>";
}
mysqli_free_result($result);

mysqli_close($conn);

?>

<p>
<hr>

<p>
<a href="EmployeesFromType.txt" >Contents</a>
of the PHP program that created this page. 	 
 
</body>
</html>