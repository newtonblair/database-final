

<html>
<head>
       <title>PHP-MySQL Final Project</title>
</head>

<body bgcolor="white">

<h3>This is a managerial query enabling you to see if a carpenter has been assigned to a maintenance request at a location they don't work at.</h3>



<hr>

<p>
Please select a carpenter:

<p>

<form action="BadMaintReqs.php" method="POST">

<select name = "emp" id="emp">
	<option disabled="disabled" selected="selected">Select An Employee</option>
	<?php

	include('connection.txt');

	$conn = mysqli_connect($server, $user, $pass, $dbname, $port)
	or die('Error connecting to MySQL server.');

	$query = "SELECT CONCAT(e.fname,' ', e.lname) AS name, c.emp_ssn 
	FROM mydb.Employee AS e JOIN mydb.Carpenter AS c ON e.ssn = c.emp_ssn 
	ORDER BY e.lname ASC;";

	$results = mysqli_query($conn, $query);

	while($row = mysqli_fetch_array($results, MYSQLI_BOTH))
	  {
	    $name = $row['name'];
	    $ssn = $row['emp_ssn'];
	    echo "<option value = '$ssn'>$name</option>";
	  }

	mysqli_free_result($result);

	mysqli_close($conn);

	?>
</select>

<input type="submit" value="submit">
</form>



<hr>

<p>
<a href="CarpReqCheck.txt" >Contents</a>.
of this page.

<p>
<a href="BadMaintReqs.txt" >Contents</a>
of the PHP page that gets called.
(And the <a href="connection.txt" >connection data</a>,
kept separately for security reasons.)




</body>
</html>