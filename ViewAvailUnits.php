<?php

include('connection.txt');

$conn = mysqli_connect($server, $user, $pass, $dbname, $port)
or die('Error connecting to MySQL server.');

?>

<html>
  <head>
    <title>Another Simple PHP-MySQL Program</title>
    <style>
      table, td {
      border: 1px solid black;
      }
      th{
        border: 1px solid black;
        height: 20px;
      }
    </style>
  </head>
  
  <body bgcolor="white">
  
  
  <hr>
  
  
<?php
$id = $_POST['location'];

$query = "SELECT CONCAT(comp.address,' ', comp.city, ',', ' ', comp.state, ' ', comp.zipcode) as location, u.unit_num, u.comp_date AS available_since
FROM mydb.Complex AS comp JOIN mydb.Unit AS u USING(complex_id)
WHERE comp.complex_id = ".$id." AND u.comp_date IS NOT NULL;";

?>

<p>
The query:
<p>
<?php
print $query;
?>

<hr>
<p>
Result of query:
<p>

<?php
$result = mysqli_query($conn, $query)
or die(mysqli_error($conn));


print "<pre>";
print "<table align='center'>";
print "<tr>";
print "<th>Location</th>";
print "<th>Unit #</th>";
print "<th>Available Since</th>";
print "</tr>";
while($row = mysqli_fetch_array($result, MYSQLI_BOTH))
  {
    print "<tr>";
    print "<th>$row[location]</th> <th>$row[unit_num]</th> <th>$row[available_since]</th>";
    print "</tr>";
  }
print "</table>";
print "</pre>";

mysqli_free_result($result);

mysqli_close($conn);

?>

<p>
<hr>

<p>
<a href="ViewAvailUnits.txt" >Contents</a>
of the PHP program that created this page. 	 
 
</body>
</html>