<?php

include('connection.txt');

$conn = mysqli_connect($server, $user, $pass, $dbname, $port)
or die('Error connecting to MySQL server.');

?>

<html>
  <head>
    <title>Another Simple PHP-MySQL Program</title>
    <style>
      table, td {
      border: 1px solid black;
      }
      th{
        border: 1px solid black;
        height: 20px;
      }
    </style>
  </head>
  
  <body bgcolor="white">
  
  
  <hr>
  
  
<?php
$emp = $_POST['emp'];

$query = "SELECT CONCAT(e.fname, ' ',  e.lname) AS name, CONCAT(comp.address,' ', comp.city, ',', ' ', comp.state, ' ', comp.zipcode) AS location, u.unit_num
FROM mydb.Employee AS e JOIN mydb.Cleaner AS c ON e.ssn = c.emp_ssn
  JOIN mydb.Unit AS u USING(emp_ssn)
  JOIN mydb.Complex AS comp USING(complex_id)
WHERE c.emp_ssn = ".$emp.";";

?>

<p>
The query:
<p>
<?php
print $query;
?>

<hr>
<p>
Result of query:
<p>

<?php
$result = mysqli_query($conn, $query)
or die(mysqli_error($conn));


print "<pre>";
print "<table align='center'>";
print "<tr>";
print "<th>Employee Name</th>";
print "<th>Location</th>";
print "<th>Unit #</th>";
print "</tr>";
while($row = mysqli_fetch_array($result, MYSQLI_BOTH))
  {
    print "<tr>";
    print "<th>$row[name]</th> <th>$row[location]</th> <th>$row[unit_num]</th>";
    print "</tr>";
  }
print "</table>";
print "</pre>";

mysqli_free_result($result);

mysqli_close($conn);

?>

<p>
<hr>

<p>
<a href="ViewCleanerSched.txt" >Contents</a>
of the PHP program that created this page.   
 
</body>
</html>