<?php

include('connection.txt');

$conn = mysqli_connect($server, $user, $pass, $dbname, $port)
or die('Error connecting to MySQL server.');

?>

<html>
  <head>
    <title>Another Simple PHP-MySQL Program</title>
    <style>
      table, td {
      border: 1px solid black;
      }
      th{
        border: 1px solid black;
        height: 20px;
      }
    </style>
  </head>
  
  <body bgcolor="white">
  
  
  <hr>
  
  
<?php
$num = $_POST['num'];

$query = "SELECT e.fname, e.lname, CONCAT(comp.address,' ', comp.city, ',', ' ', comp.state, ' ', comp.zipcode) as location
FROM mydb.Employee AS e JOIN mydb.Carpenter as c ON e.ssn = c.emp_ssn
  JOIN mydb.Works AS w USING(emp_ssn)
    JOIN mydb.Complex AS comp USING(complex_id)
    JOIN(
    SELECT c1.emp_ssn, COUNT(w1.complex_id) As number_locations
    FROM mydb.Carpenter as c1 JOIN mydb.Works AS w1 USING(emp_ssn)
    GROUP BY c1.emp_ssn
    HAVING number_locations >= ".$num.
    ") AS people ON c.emp_ssn = people.emp_ssn
ORDER BY e.lname ASC;";


?>

<p>
The query:
<p>
<?php
print $query;
?>

<hr>
<p>
Result of query:
<p>

<?php
$result = mysqli_query($conn, $query)
or die(mysqli_error($conn));


print "<pre>";
print "<table align='center'>";
print "<tr>";
print "<th>Firstname</th>";
print "<th>Lastname</th>";
print "<th>Locations</th>";
print "</tr>";
while($row = mysqli_fetch_array($result, MYSQLI_BOTH))
  {
    print "<tr>";
    print "<th>$row[fname]</th> <th>$row[lname]</th> <th>$row[location]</th>";
    print "</tr>";
  }
print "</table>";
print "</pre>";

mysqli_free_result($result);

mysqli_close($conn);

?>

<p>
<hr>

<p>
<a href="ViewCarpLocations.txt" >Contents</a>
of the PHP program that created this page. 	 
 
</body>
</html>