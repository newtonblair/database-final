

<html>
<head>
       <title>PHP-MySQL Final Project</title>
</head>

<body bgcolor="white">

<h3>This is a managerial query that allows you to view which carpenters work at x locations and view which locations they work at.</h3>



<hr>

<p>
Please select a numerical value to see which carpenters work at at least that many locations, And view which locations they work at.

<p>

<form action="ViewCarpLocations.php" method="POST">

<select name = "num" id="num">
	<option disabled="disabled" selected="selected">Select A Numerical Value</option>
	<?php

	include('connection.txt');

	$conn = mysqli_connect($server, $user, $pass, $dbname, $port)
	or die('Error connecting to MySQL server.');

	$query = "SELECT DISTINCT COUNT(w.complex_id) AS num_locations FROM mydb.Carpenter AS c JOIN mydb.Works AS w USING(emp_ssn) GROUP BY c.emp_ssn ORDER BY num_locations ASC;";

	$results = mysqli_query($conn, $query);

	while($row = mysqli_fetch_array($results, MYSQLI_BOTH))
	  {
	    $num_locations = $row['num_locations'];
	    echo "<option value = '$num_locations'>$num_locations</option>";
	  }

	mysqli_free_result($result);

	mysqli_close($conn);

	?>
</select>

<input type="submit" value="submit">
</form>



<hr>

<p>
<a href="CarpentersXLocations.txt" >Contents</a>.
of this page.

<p>
<a href="ViewCarpLocations.txt" >Contents</a>
of the PHP page that gets called.
(And the <a href="connection.txt" >connection data</a>,
kept separately for security reasons.)




</body>
</html>